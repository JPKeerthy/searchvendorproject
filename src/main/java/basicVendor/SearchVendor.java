package basicVendor;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class SearchVendor {
		@Test	 
	public void vendor() {
		 
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get("https://acme-test.uipath.com/account/login");
	    driver.findElementById("email").sendKeys("jpriyakeerthy@gmail.com");
	    driver.findElementById("password").sendKeys("welcome");
	    driver.findElementById("buttonLogin").click();
		
		//Actions
		Actions builder = new Actions(driver);
		WebElement vendor = driver.findElementByXPath("(//i)/following::button[4]");
		
		builder.moveToElement(vendor).pause(3000).perform();
		driver.findElementByPartialLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("RO254678");
		driver.findElementById("buttonSearch").click();
		 WebElement table = driver.findElementByXPath("//div/table/tbody/tr[2]/td[1]");
		 String text = table.getText();
		 System.out.println(text);
		
		
}
}
