package vendor.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.framework.design.ProjectMethods;

public class SearchResultsPage extends ProjectMethods {
 public SearchResultsPage() {
	 PageFactory.initElements(driver, this);
 }
 @FindBy(how=How.XPATH,using="//div/table/tbody/tr[2]/td[1]") WebElement eleVerify;
 public SearchResultsPage verifyVendor() {
	 return this;
 }
}
