package vendor.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.framework.design.ProjectMethods;

public class SearchPage extends ProjectMethods{

	public SearchPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="vendorTaxID") WebElement eleID;
	@FindBy(how=How.ID,using="buttonSearch") WebElement eleClkSearch;
	
	public SearchPage enterVendorID(String data) {
		clearAndType(eleID, data);
		return this;
	}
	
	public SearchResultsPage clickSearchVendor() {
	click(eleClkSearch);
	return new SearchResultsPage();
	}
}
