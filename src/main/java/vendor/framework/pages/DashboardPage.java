package vendor.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import vendor.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods {
	

	
	public DashboardPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how = How.XPATH,using="(//i)/following::button[4]") WebElement elevendor;
	@FindBy(how=How.LINK_TEXT,using = "Search for Vendor") WebElement elesearch;
	
	public SearchPage hoverVendor() {
		move(elevendor, elesearch);
		return new SearchPage();
	}
	
	
	

}
