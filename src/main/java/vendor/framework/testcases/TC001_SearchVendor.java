package vendor.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import vendor.framework.design.ProjectMethods;
import vendor.framework.pages.LoginPage;

public class TC001_SearchVendor extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="search";
		testNodes="search vendors";
		author="JP";
		category="smoke";
		dataSheetName="TC001_Login";
	}
	
	@Test(dataProvider="fetchData")
	public void searchVendor(String email, String password, String tax) {
		new LoginPage()
		.enterEmail(email)
		.enterPassword(password)
		.clickLogin()
		.hoverVendor()
		.enterVendorID(tax)
		.clickSearchVendor()
		.verifyVendor();
		
	}

}
